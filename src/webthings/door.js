'use strict';
const {
  Property,
  Thing,
  Value,
} = require('webthing');

class WebThingDoorSensor extends Thing {
  constructor(trigger, name) {
    super(`urn:dev:ops:${trigger}`, name, ['DoorSensor']);
    
    this.trigger = trigger;
    this.doorOpen = new Value(true);

    this.addProperty(
      new Property(this,
        'open',
        this.doorOpen,
        {
          '@type': 'OpenProperty',
          readOnly: true,
          title: 'open',
          type: 'boolean',
        }
      )
    );
  }

  setValue = (open) => {
    this.doorOpen.notifyOfExternalUpdate(open);
    console.log(this.trigger, open);
  }

  updateProperties() {
    const properties = getProperties();
    for(const key in properties) {
      this[key].notifyOfExternalUpdate(properties[key]);
    }
  }
}

module.exports = WebThingDoorSensor;
