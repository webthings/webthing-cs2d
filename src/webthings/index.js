'use strict';
const {
  MultipleThings,
  WebThingServer,
} = require('webthing');
const DoorThing = require('./door');
const LightThing = require('./light');
const SmokeThing = require('./smoke');

async function runServer(port, ingress, egress) {
  
  const doors = [
    {
      name: 'Entrance',
      trigger: 'door_entrance',
    },
    {
      name: 'Backdoor',
      trigger: 'door_garden',
    },
    {
      name: 'Bathroom',
      trigger: 'door_bathroom',
    },
  ];

  const lights = [
    {
      name: 'Living room',
      trigger: 'light_living',
    },
    {
      name: 'Bathroom',
      trigger: 'light_bathroom',
    },
    {
      name: 'Kitchen',
      trigger: 'light_kitchen',
    },
  ];

  const smokes = [
    {
      name: 'Kitchen',
      trigger: 'smoke_kitchen',
    },
  ];

  for (const door of doors) {
    door.thing = new DoorThing(door.trigger, door.name);
  }
  for (const light of lights) {
    const callback = async () => {
      console.log(`light wants to change ${light.trigger}`);
      await egress.write(`trigger ${light.trigger}`);
    };
    light.thing = new LightThing(light.trigger, light.name, callback);
  }
  for (const smoke of smokes) {
    smoke.thing = new SmokeThing(smoke.trigger, smoke.name);
  }

  ingress.on('trigger', ({name, state}) => {
    for (const door of [...doors, ...lights, ...smokes]) {
      if (door.trigger === name) {
        const open = state === 'open';
        door.thing.setValue(open);
      }
    }
  });
  
  const server = new WebThingServer(new MultipleThings([
    ...doors.map(door => door.thing),
    ...lights.map(light => light.thing),
    ...smokes.map(smoke => smoke.thing),
  ], 'House'), port);

  process.on('SIGINT', async () => {
    try {
      await egress.close();
      ingress.close();
      server.stop();
    } finally {
      process.exit();
    }
  });

  server.start();
}

module.exports = runServer;
