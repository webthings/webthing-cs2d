'use strict';
const {
  Property,
  Thing,
  Value,
} = require('webthing');

// const Output = require('./rcon-output');

class WebThingDoorSensor extends Thing {
  constructor(trigger, name, callback) {
    super(`urn:dev:ops:${trigger}`, name, ['Light']);

    this.trigger = trigger;
    this.lightOn = new Value(false, callback);

    this.addProperty(
      new Property(this,
        'on',
        this.lightOn,
        {
          '@type': 'OnOffProperty',
          title: 'on',
          type: 'boolean',
        }
      )
    );
  }

  setValue = (open) => {
    this.lightOn.notifyOfExternalUpdate(open);
    console.log('setValue of thing: '+this.trigger, open);
  }

  updateProperties() {
    const properties = getProperties();
    for(const key in properties) {
      this[key].notifyOfExternalUpdate(properties[key]);
    }
  }
}

module.exports = WebThingDoorSensor;
