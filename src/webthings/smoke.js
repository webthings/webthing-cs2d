'use strict';
const {
  Property,
  Thing,
  Value,
} = require('webthing');

class WebThingDoorSensor extends Thing {
  constructor(trigger, name) {
    super(`urn:dev:ops:${trigger}`, name, ['SmokeSensor']);
    
    this.trigger = trigger;
    this.smokeDetected = new Value(false);

    this.addProperty(
      new Property(this,
        'smoke',
        this.smokeDetected,
        {
          '@type': 'SmokeProperty',
          readOnly: true,
          title: 'smoke',
          type: 'boolean',
        }
      )
    );
  }

  setValue = (open) => {
    this.smokeDetected.notifyOfExternalUpdate(open);
    console.log(this.trigger, open);
  }

  updateProperties() {
    const properties = getProperties();
    for(const key in properties) {
      this[key].notifyOfExternalUpdate(properties[key]);
    }
  }
}

module.exports = WebThingDoorSensor;
