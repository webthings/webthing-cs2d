'use strict';
const dgram = require('dgram');

class RconOutput {
    constructor() {
        this.initialized = false;
        this.socket = null;
        this.rconPassword = process.env.RCON_PASSWORD;
    }

    init = () => {
        const socket = dgram.createSocket('udp4');
        this.socket = socket;
        console.log('Connecting to ', parseInt(process.env.RCON_PORT || 36963), process.env.RCON_HOST || '127.0.0.1');
        socket.connect(parseInt(process.env.RCON_PORT || 36963), process.env.RCON_HOST || '127.0.0.1');

        socket.on('connect', () => {
            this.initialized = true;
        });

        socket.on('message', (msg) => {
            console.log('message', msg);
        });

        socket.on('listening', () => {
            console.log('listening', socket.address());
        });

        socket.on('error', (err) => {
            console.error('Error', err);
        });
    }

    write = async (command) => {
        if (!this.initialized) throw Error('Not initialized');
        const cmdLength = Buffer.alloc(2);
        cmdLength.writeUInt16LE(command.length);

        const msg = [
            Buffer.from([1, 0, 242]),
            Buffer.from([this.rconPassword.length]),
            Buffer.from(this.rconPassword),
            cmdLength,
            Buffer.from(command),
        ];
        this.socket.send(msg);
    }

    close = async () => {
        return new Promise((resolve) => {
            this.socket.close(resolve);
        })
    }
}

module.exports = RconOutput;
