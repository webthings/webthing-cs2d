'use strict';
const EventEmitter = require('events');
const express = require('express');

class HttpIngress extends EventEmitter {
    constructor() {
        super();
        this.app = express();
        this.app.get('/say', (req, res) => {
            this.emit('say', req.query);
            res.send('');
        });

        this.app.get('/trigger', (req, res) => {
            this.emit('trigger', {
                name: req.query.name,
                state: req.query.state,
            });
            res.send('');
        })

        this.app.listen(3000);
    }

    close() {
        this.app.close();
    }
}

module.exports = HttpIngress;
