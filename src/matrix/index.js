const {
    MatrixClient,
    SimpleFsStorageProvider,
} = require('matrix-bot-sdk');

const leaveReasons = {
    '0': 'normal leave (player left game manually)',
    '1': 'ping timeout',
    '2': 'kicked (without reason text)',
    '3': 'cheat detected',
    '4': 'server shutdown',
    '5': 'client confirmed server shutdown (each client sends this when the server is being shut down)',
    '6': 'banned',
    '7': 'error during join process',
    '8': 'client failed to load map',
    '9': 'join process cancelled by user',
    '10': 'ping too high (mp_pinglimit)',
    '11': 'kicked for too many team kills (mp_teamkillpenalty)',
    '12': 'kicked for too many hostage kills (mp_hostagepenalty)',
    '13': 'kicked via voting of other players (mp_kickpercent, vote)',
    '14': 'kicked for reservation system (mp_reservations)',
    '15': 'speedhack detected',
    '16': 'kicked (with reason text)',
    '17': 'player has been rerouted to another server (reroute)',
    '18': 'kicked for being idle (mp_idletime, mp_idlekick, mp_idleaction)',
    '19': 'kicked for flooding chat with messages',
};

function run (ingress, egress) {
    const storage = new SimpleFsStorageProvider(`${process.env.FOLDER_DATA}/matrix-client-storage.json`);
    const client = new MatrixClient(process.env.MATRIX_HOMESERVER, process.env.MATRIX_ACCESSTOKEN, storage);

    ingress.on('say', ({ id, text }) => {
        client.sendText(process.env.MATRIX_ADMINROOM, `${id}: ${text}`);
    });

    ingress.on('join', ({ id, name }) => {
        client.sendNotice(process.env.MATRIX_ADMINROOM, `${name} joined. (id: ${id})`);
    });

    ingress.on('leave', ({ id, reason }) => {
        const reasonString = leaveReasons[reason];
        client.sendNotice(process.env.MATRIX_ADMINROOM, `${id} left.${reason > 0 ? ` (reason: ${reasonString || reason})` : ''}`);
    });
}

module.exports = run;
