'use strict';
const WebThings = require('./webthings');
const RconOutput = require('./rcon-egress');
const egress = new RconOutput();
egress.init();

// const HttpIngress = require('./http-ingress');
// const ingress = new HttpIngress(3000);
const FifoIngress = require('./fifo-ingress');
const ingress = new FifoIngress(process.env.FIFO_PATH);

WebThings(8000, ingress, egress);
