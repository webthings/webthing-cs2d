'use strict';
const EventEmitter = require('events');
const fs = require('fs');

class FifoIngress extends EventEmitter {
    constructor(path) {
        super();
        this.path = path;
        this.ingress = fs.createReadStream(path);
        this.setUpListeners();
    }

    setUpListeners() {
        this.ingress.on('error', () => {
            console.log('ingress had an error');
        });
        this.ingress.on('end', () => {
            console.log('ingress ended');
            this.ingress = fs.createReadStream(this.path);
            this.setUpListeners();
        });
        this.ingress.on('data', (data) => {
            data = data.toString();
            const lines = data.split(/\r?\n/g);
            for (const line of lines) {
                if (line === '') continue;
                const info = line.split('\t');
                console.log(info);
                if (info[0] === 'trigger') {
                    this.emit('trigger', {
                        name: info[1],
                        state: info[2],
                    });
                }
                if (info[0] === 'say') {
                    this.emit('say', {
                        id: info[1],
                        text: info[2],
                    });
                }
                if (info[0] === 'join') {
                    this.emit('join', {
                        id: info[1],
                        name: info[2],
                    });
                }
                if (info[0] === 'leave') {
                    this.emit('leave', {
                        id: info[1],
                        reason: info[2],
                    });
                }
            }
        });
    }

    close() {
        //TODO
    }
}

module.exports = FifoIngress;
