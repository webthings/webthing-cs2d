# Home Automation in CS2D

Make your home in [CS2D](https://cs2d.com) WebThings-compatible.

![A player interacts with lights, doors and a fire oven in the video game CS2D. Their actions get shown in real time in the WebThings gateway.](https://gateway.pinata.cloud/ipfs/QmY9hGHVqg8JCYXPj4tEW2Qpma5j7uoqxBjh1iAFPrbC3j/cs2d-webthings.webm)

## Prerequisites

* A Linux machine to run the CS2D server on
* NodeJS and NPM for the WebThings

## Get it running

### CS2D server
1. [Download the CS2D server](https://cs2d.com/download.php). You need the Linux client for the assets and dedicated server. Extract them into one folder.
2. Create a named pipe at `<CS2D root>/sys/cs2d-to-node`. Run `mkfifo sys/cs2d-to-node`.
3. Copy the files in `./maps` into `<CS2D root>/maps`.
4. Copy the files in `./cs2d_lua` into `<CS2D root>/sys/lua`. Replace the existing file.
5. Adapt the config at `<CS2D root>/sys/server.cfg`. To use the map: `sv_map house`. To have no time limit: `mp_timelimit 0`. To avoid being in the public server list: `sv_lan 1`. Set a RCON password using `sv_rcon your_password`.
6. Adapt the `rconPassword` variable in `src/rcon-output.js` to your password.
7. Ensure the server is executable. Run `chmod +x cs2d_dedicated`.
8. Run the server. `./cs2d_dedicated`

### WebThing server

This must run on the same machine as the CS2D server.

1. Download this repo.
```
git clone git@gitlab.com:webthings/webthing-cs2d.git
cd webthing-cs2d
npm install
```
2. Change the path to the named pipe in `src/standalone.js`.
3. Run it `npm run`. It needs to be restarted every time the CS2D server restarts.

### CS2D client

The client can run on a different machine than the CS2D server and the WebThings server.

1. Start the game. I recommend running it on Steam with the Steam Compatibility setting on the latest version of Proton.
2. Join your server. If you're trying this locally, connect to `localhost:36963`.
3. Join a team and enjoy the house.

### Webthings gateway

You can use a [WebThings gateway]() running on a Raspberry Pi or quickly install it on any computer.

If you have Docker installed:

```sh
mkdir -p data
docker run \
    --detached \
    -e TZ=Europe/Berlin \
    -v $(pwd)/data:/home/node/.webthings \
    --network="host" \
    --log-opt max-size=1m \
    --log-opt max-file=10 \
    --name webthings-gateway \
    webthingsio/gateway:latest
```

## Build your own map

CS2D has a built-in map editor. It's easy to create your own map for this project.

1. Create entities with names.
2. Place some buttons that trigger those names.
3. Register all names of lights, doors and smoke sources in `cs2d_lua/server.lua` and `src/standalone.js`.
