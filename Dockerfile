FROM node:current-alpine

ENV NODE_ENV production
ENV FOLDER_DATA /var/cs2d-webthing

RUN mkdir -p /var/cs2d-webthing

WORKDIR "/app"

# Install app dependencies
COPY package.json /app/
RUN npm install --production

# Bundle app source
COPY . /app

EXPOSE 8000
EXPOSE 5353/udp
CMD ["npm", "start"]
