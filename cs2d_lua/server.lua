local doors = {}
doors["door_entrance"] = false
doors["door_bathroom"] = false
doors["door_garden"] = false
doors["light_bathroom"] = false
doors["light_living"] = false
doors["light_kitchen"] = false
doors["smoke_kitchen"] = true

local egress = io.open("sys/cs2d-to-node","a")

addhook("trigger","trigger")
function trigger(name)
    for key,value in pairs(doors) do
        if (name == key) then
            value = not value
            doors[key] = value
            if (value) then
                egress:write(key .. " open\n")
                egress:flush()
            else
                egress:write(key .. " closed\n")
                egress:flush()
            end
        end
    end
end
